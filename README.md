Пример сборки и запуска проекта hard/command-line:
````
cd /hard/command-line
mvn package
java -jar command-line-1.0.jar input.txt
````

Для остальных сборка и запуск по шаблону. 
````
cd /easy/3-palindrome
mvn package
java -jar palindrome-1.0.jar
````

WebService доступен по адресу http://127.0.0.1:8080/search