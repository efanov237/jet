import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;

//
// Определить функции как функторы
// Map<String, Operation> operations = new HashMap<>();
// operations.put("ACK", Functions::ackermann);
// operations.put("F", Functions::factorial);
// operations.put("FIB", Functions::fibonacci);
// ....
// написать компаратор и использовать в потоке
// пока не удалось
public class FileParser {

    private final String INPUT_FILE;
    private final String OUTPUT_FILE;

    private List<String> results = new ArrayList<>();
    private Long counter = 0L;

    public FileParser(String input_file, String output_file) {
        INPUT_FILE = input_file;
        OUTPUT_FILE = output_file;
    }

    public void parse() {
        try {
            Files
                    .lines(Paths.get(INPUT_FILE))
                    .map(line -> Arrays.asList(line.split(" ")))
                    .forEach(this::compute);
            Files.write(Paths.get(OUTPUT_FILE), results);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void compute(List<String> line) {
        counter++;
        switch (line.get(0)) {
            case "ACK":
                results.add(counter + " " + String.valueOf(Functions.ackermann(Integer.parseInt(line.get(1)), Integer.parseInt(line.get(2)))));
                break;
            case "F":
                results.add(counter + " " + String.valueOf(Functions.factorial(Integer.parseInt(line.get(1)))));
                break;
            case "FIB":
                results.add(counter + " " + String.valueOf(Functions.fibonacci(Integer.parseInt(line.get(1)))));
                break;
                default:
                    System.err.println("Invalid input");
        }
    }
}
