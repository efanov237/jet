public class Main {
    public static void main(String[] args) {
        if (args.length != 1) {
            System.err.println("Invalid args");
            System.exit(0);
        }

        FileParser parser = new FileParser(args[0], "output.txt");
        parser.parse();
    }
}
