import java.util.function.*;

public class Functions {


    public static long ackermann(int a, int b) {

        return (a == 0) ?
                b + 1 :
                ((a > 0) && (b == 0)) ?
                        ackermann(a - 1, 1) :
                        ackermann(a - 1, (int) ackermann(a, b - 1));
    }

    public static long factorial(int a) {
        Recursive<IntToLongFunction> recursive = new Recursive<>();
        recursive.func = x -> (x == 0) ? 1 : x * recursive.func.applyAsLong(x - 1);
        return recursive.func.applyAsLong(a);
    }

    public static long fibonacci(int a) {
        Recursive<IntToLongFunction> recursive = new Recursive<>();
        recursive.func = x -> x == 1 ? 1 : x == 2 ? 1 : recursive.func.applyAsLong(x - 1) + recursive.func.applyAsLong(x - 2);
        return recursive.func.applyAsLong(a);
    }

}

