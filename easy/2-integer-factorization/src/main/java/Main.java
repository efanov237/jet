import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Main {
    public static List<Integer> findFactors(int number) {
        int i = 2;
        List<Integer> factors = new ArrayList<>();
        while ((i * i) <= number) {
            while ((number % i) == 0) {
                factors.add(i);
                number /= i;
            }
            i++;
        }
        if (number > 1) {
            factors.add(number);
        }
        return factors;
    }

    public static void main(String[] args) {
        Random rand = new Random();
        int number = rand.nextInt(100);
        System.out.println("Factors " + number + " is:");
        for (int i : findFactors(number)) {
            System.out.print(i + " ");
        }
    }
}
