import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;


public class findFactorsTests {

    @Test
    public void checkPrimeNumber() {
        List<Integer> factors = new ArrayList<>();
        factors.add(17);
        Assert.assertEquals( factors, Main.findFactors(17));
    }


    @Test
    public void checkCompositeNumber() {
        List<Integer> factors = new ArrayList<>();
        factors.add(2);
        factors.add(3);
        factors.add(7);
        Assert.assertEquals(factors, Main.findFactors(42));

    }

}