import org.junit.Test;
import static org.junit.Assert.assertEquals;

public class SecondLargestTests {

    @Test
    public void withNegative() {
        int[] arr = {-1, 8, -17, -6, -3};
        assertEquals(Main.getSecondLargest(arr), -1);
    }

    @Test
    public void ordered() {
        int[] arr = {1, 2, 3, 4, 5};
        assertEquals(Main.getSecondLargest(arr), 4);
    }
}