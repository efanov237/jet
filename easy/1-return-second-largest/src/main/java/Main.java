import java.util.Random;

public class Main {


    public static int getSecondLargest(int[] sequence) {
        int max = Integer.MIN_VALUE;
        int secondMax = Integer.MIN_VALUE;
        for (int number : sequence) {
            if (number > max) {
                max = number;
            }
        }
        for (int number : sequence) {
            if ((number > secondMax) && (number < max)) {
                secondMax = number;
            }
        }
        return secondMax;
    }

    public static void main(String[] args) {
        System.out.println("Input sequence: ");
        Random rand = new Random();
        int[] arr = new int[10];
        for (int i = 0; i < 10; i++) {
            int num = rand.nextInt(100);
            arr[i] = num;
            System.out.print(num + " ");
        }
        System.out.println();
        System.out.println("Second largest number is " + getSecondLargest(arr));
    }
}

