import org.junit.Assert;
import org.junit.Test;

public class substringFinderTests {

    @Test
    public void substringInString() {
        String string = "Hello world";
        String substring = "world";
        Assert.assertEquals(6, Main.findSubstring(string, substring));
    }

    @Test
    public void substringNotInString() {
        String string = "Hello world";
        String substring = "none";
        Assert.assertEquals(-1, Main.findSubstring(string, substring));
    }

    @Test
    public void substringLongerString() {
        String string = "Hello world";
        String substring = "very long substring";
        Assert.assertEquals(-1, Main.findSubstring(string, substring));
    }
}
