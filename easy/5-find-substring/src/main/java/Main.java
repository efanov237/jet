import java.util.Scanner;

public class Main {
    public static int findSubstring(String string, String substring) {
        string = string.toLowerCase();
        substring = substring.toLowerCase();
        for (int i = 0; i < string.length(); i++) {
            int a = i;
            for (int j = 0; j < substring.length(); j++) {
                if (substring.charAt(j) == string.charAt(a)) {
                    if (j == substring.length() - 1)
                        return i;
                    a++;
                } else {
                    break;
                }
            }
        }
        return -1;
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Search in: ");
        String string = sc.nextLine();
        System.out.print("\nSearch substring: ");
        String substring = sc.nextLine();
        int res = findSubstring(string, substring);
        if (res >= 0) {
            System.out.println("Found: " + res);
        } else {
            System.out.println("Not found");
        }
    }
}
