import java.util.Scanner;

public class Main {
    public static boolean isPalindrome(String string) {
        String preparedString = string.toLowerCase().replaceAll("\\s+", "");
        int forward = 0;
        int reverse = preparedString.length() - 1;
        while (forward < reverse) {
            if (preparedString.charAt(forward) != preparedString.charAt(reverse)) {
                return false;
            }
            forward++;
            reverse--;
        }
        return true;
    }

    public static void main(String[] args) {
        String input;
        Scanner sc = new Scanner(System.in);
        input = sc.nextLine();
        if (isPalindrome(input)) {
            System.out.println("palindrome");
        } else {
            System.out.println("not palindrome");
        }
    }
}
