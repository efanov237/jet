import org.junit.Assert;
import org.junit.Test;

public class palindromeCheckerTest {

    @Test
    public void checkPalindrome() {
        String palindrome = "Never Odd or Even";
        Assert.assertTrue(Main.isPalindrome(palindrome));
    }

    @Test
    public void checkNotPalindrome() {
        String notPalindrome = "is no palindrome";
        Assert.assertFalse(Main.isPalindrome(notPalindrome));
    }
}