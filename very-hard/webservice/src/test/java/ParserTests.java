import efanov237.jet.veryhard.Application;
import efanov237.jet.veryhard.common.XmlLinkParser;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = Application.class)
public class ParserTests {
    @Autowired
    XmlLinkParser parser;
    @Test
    public void urlFieldParse() throws IOException, SAXException, ParserConfigurationException {
        String xml = "<test><unnecessary><url>success</url></unnecessary></test>";
        Assert.assertEquals("success", parser.parse(xml).get(0));
    }
}
