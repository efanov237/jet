import efanov237.jet.veryhard.Application;
import efanov237.jet.veryhard.model.DomainStat;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = Application.class)
public class DomainStatTests {

    @Test
    public void repeatableLinks() {
        DomainStat stat = new DomainStat();
        stat.add("hostname.ru", "http://hostname.ru/123");
        stat.add("hostname.ru", "http://hostname.ru/123");
        Assert.assertEquals(1, stat.getAsMap().get("hostname.ru").size());
    }
}
