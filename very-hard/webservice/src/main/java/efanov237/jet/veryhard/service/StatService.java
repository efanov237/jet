package efanov237.jet.veryhard.service;

import efanov237.jet.veryhard.model.DomainStat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;


@Service
public class StatService {

    private final SearchService searchService;

    @Autowired
    public StatService(SearchService searchService) {
        this.searchService = searchService;
    }

    public DomainStat getStat(List<String> keywordList) {
        searchService.setDomainStat(new DomainStat());
        return searchService.search(keywordList);
    }
}
