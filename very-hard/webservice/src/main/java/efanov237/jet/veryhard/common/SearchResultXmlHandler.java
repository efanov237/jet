package efanov237.jet.veryhard.common;

import org.springframework.stereotype.Component;
import org.xml.sax.Attributes;
import org.xml.sax.helpers.DefaultHandler;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

@Component
public class SearchResultXmlHandler extends DefaultHandler {

    private String link;
    private List<String> linkList = new CopyOnWriteArrayList<>();

    private boolean isLink = false;

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) {
        if (qName.equalsIgnoreCase("url")) {
            isLink = true;
        }
    }

    @Override
    public void characters(char[] ch, int start, int length) {
        if (isLink) {
            link = new String(ch, start, length);
            isLink = false;
        }
    }

    @Override
    public void endElement(String uri, String localName, String qName) {
        if (qName.equalsIgnoreCase("url")) {
            linkList.add(link);
        }
    }

    public List<String> getLinkList() {
        return linkList;
    }

    public void clear() {
        linkList = new ArrayList<>();
    }
}
