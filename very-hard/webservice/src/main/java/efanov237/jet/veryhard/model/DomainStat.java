package efanov237.jet.veryhard.model;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import efanov237.jet.veryhard.common.StatSerializer;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArraySet;

@Component
@JsonSerialize(using = StatSerializer.class)
public class DomainStat {

    private final Map<String, Set<String>> stat;

    public DomainStat() {
        this.stat = new ConcurrentHashMap<>();
    }

    public void add(String hostName, String link) {
        if (stat.containsKey(hostName)) {
            stat.get(hostName).add(link);
        } else {
            Set<String> links = new CopyOnWriteArraySet<>();
            links.add(link);
            stat.put(hostName, links);
        }
    }

    public Map<String, Set<String>> getAsMap() {
        return stat;
    }
}
