package efanov237.jet.veryhard.service;

import efanov237.jet.veryhard.common.XmlLinkParser;
import efanov237.jet.veryhard.model.DomainStat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

@Component
public class SearchService {

    private final ThreadPoolTaskExecutor executor;

    private final XmlLinkParser parser;
    private DomainStat domainStat;

    @Autowired
    public SearchService(ThreadPoolTaskExecutor executor, XmlLinkParser parser) {
        this.executor = executor;
        this.parser = parser;
    }

    @Autowired
    public void setDomainStat(DomainStat domainStat) {
        this.domainStat = domainStat;
    }


    public DomainStat search(List<String> keywordList) {
        Collection<Future<?>> futures = new ArrayList<>();
        for (String keyword : keywordList) {
            RequestThread request = new RequestThread(parser);
            request.setDomainStat(domainStat);
            request.setKeyword(keyword);
            futures.add(executor.submit(request));
        }
        for (Future<?> future : futures) {
            try {
                future.get();
            } catch (ExecutionException | InterruptedException e) {
                e.printStackTrace();
            }
        }
        return this.domainStat;
    }

}