package efanov237.jet.veryhard.common;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.IOException;
import java.io.StringReader;
import java.util.List;

@Component
public class XmlLinkParser {

    private final SearchResultXmlHandler handler;

    @Autowired
    public XmlLinkParser(SearchResultXmlHandler handler) {
        this.handler = handler;
    }


    public List<String> parse(String xml) throws ParserConfigurationException, SAXException, IOException {
        List<String> result;
        SAXParserFactory factory = SAXParserFactory.newInstance();
        SAXParser saxParser = factory.newSAXParser();
        saxParser.parse(new InputSource(new StringReader(xml)), handler);
        result = handler.getLinkList();
        handler.clear();
        return result;
    }

}
