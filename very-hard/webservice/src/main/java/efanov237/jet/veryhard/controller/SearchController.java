package efanov237.jet.veryhard.controller;

import efanov237.jet.veryhard.model.DomainStat;
import efanov237.jet.veryhard.service.StatService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/search", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
public class SearchController {

    private final StatService statService;

    @Autowired
    public SearchController(StatService statService) {
        this.statService = statService;
    }

    @GetMapping
    public ResponseEntity<DomainStat> search(@RequestParam("query") List<String> keywords) {
        return new ResponseEntity<>(statService.getStat(keywords), HttpStatus.OK);
    }
}
