package efanov237.jet.veryhard.service;

import com.google.common.net.InternetDomainName;
import efanov237.jet.veryhard.common.XmlLinkParser;
import efanov237.jet.veryhard.model.DomainStat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.net.URL;

@Component
public class RequestThread implements Runnable {
    private final Logger logger = LoggerFactory.getLogger(RequestThread.class);

    private final XmlLinkParser parser;

    private final String API_KEY = "03.121479983:a317db25d1e6a5e7e5fddc2cbd802663";
    private final String USER = "maksim2731";

    private String keyword;
    private DomainStat domainStat;

    @Autowired
    public RequestThread(XmlLinkParser parser) {
        this.parser = parser;
    }

    public void setDomainStat(DomainStat domainStat) {
        this.domainStat = domainStat;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    @Override
    public void run() {
        logger.debug("Start '" + keyword + "' thread");
        UriComponents url = UriComponentsBuilder.newInstance()
                .scheme("https")
                .host("yandex.com")
                .path("/search/xml")
                .queryParam("user", USER)
                .queryParam("query", keyword)
                .queryParam("key", API_KEY)
                .queryParam("groupby", "attr%3D\"\".mode%3Dflat.groups-on-page%3D10.docs-in-group%3D1")
                .build().encode();

        RestTemplate restTemplate = new RestTemplate();
        String xmlResponse = restTemplate.getForObject(url.toUri(), String.class);
        try {
            for (String link : parser.parse(xmlResponse)) {
                String domain = InternetDomainName.from(new URL(link).getHost()).topPrivateDomain().toString();
                domainStat.add(domain, link);
            }
            logger.debug("Down '" + keyword + "' thread");
        } catch (ParserConfigurationException | SAXException | IOException e) {
            e.printStackTrace();
        }
    }
}
