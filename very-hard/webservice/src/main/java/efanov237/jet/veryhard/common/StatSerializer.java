package efanov237.jet.veryhard.common;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import efanov237.jet.veryhard.model.DomainStat;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.Map;
import java.util.Set;

@Component
public class StatSerializer extends StdSerializer<DomainStat> {

    protected StatSerializer() {
        super(DomainStat.class);
    }

    @Override
    public void serialize(DomainStat domainStat, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
        Set<Map.Entry<String, Set<String>>> stat = domainStat.getAsMap().entrySet();
        jsonGenerator.writeStartObject();
        for (Map.Entry<String, Set<String>> entry : stat) {
            jsonGenerator.writeStringField(entry.getKey(), String.valueOf(entry.getValue().size()));
        }
        jsonGenerator.writeEndObject();
    }
}
