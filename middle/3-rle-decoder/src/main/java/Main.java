import java.util.Scanner;

public class Main {
    public static String decode(String source) {
        int count = 0;
        StringBuilder result = new StringBuilder () ;
        for (int i = 0; i < source.length(); i++) {
            char c = source.charAt(i);
            if (Character.isDigit(c)) {
                count = count * 10 + c - '0' ;
            } else {
                while (count >0){
                    result.append(c);
                    count--;
                }
            }
        }
        return result.toString();
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println(decode(sc.nextLine()));
    }
}
