import org.junit.Assert;
import org.junit.Test;

public class MainTest {

    @Test
    public void decode() {
        Assert.assertEquals("aaabccdd", Main.decode("3a1b2c2d"));
    }
}