import org.junit.Assert;
import org.junit.Test;

public class RleEncodeTests {
    @Test
    public void encodeTest() {
        String string = "aaabccdd";
        Assert.assertEquals("3a1b2c2d", Main.encode(string));
    }
}
