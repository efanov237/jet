import java.util.Scanner;

public class Main {
    public static String encode(String string) {
        StringBuilder code = new StringBuilder();
        for (int i = 0; i < string.length(); i++) {
            int runLength = 1;
            while ((i + 1 < string.length()) && (string.charAt(i) == string.charAt(i + 1))) {
                runLength++;
                i++;
            }
            code.append(runLength);
            code.append(string.charAt(i));
        }
        return code.toString();
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println(encode(sc.nextLine()));
    }
}
