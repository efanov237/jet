import java.io.IOException;
import java.util.Random;

public class Main {


    private static void quickSortDescending(int[] array, int lo, int hi) {
        if (lo < hi) {
            int q = partition(array, lo, hi);
            quickSortDescending(array, lo, q);
            quickSortDescending(array, q + 1, hi);
        }
    }


    private static int partition(int[] array, int lo, int hi) {
        int pivot = array[lo];
        int i = lo;
        int j = hi;
        while (true) {
            while (array[i] > pivot) {
                i++;
            }
            while (array[j] < pivot) {
                j--;
            }
            if (i < j) {
                int temp = array[i];
                array[i] = array[j];
                array[j] = temp;
                i++;
                j--;
            } else {
                return j;
            }
        }
    }


    public static int getLargest(int[] arr, int N) {
        quickSortDescending(arr, 0, arr.length-1);
        return arr[N - 1];
    }


    public static void main(String[] args) {
        Random rand = new Random();
        int[] arr = new int[10];
        for (int i = 0; i < 10; i++) {
            int num = rand.nextInt(100);
            arr[i] = num;
            System.out.print(num + " ");
        }
        int N = rand.nextInt(5) + 1;
        System.out.println("\n" + N + " largest: " + getLargest(arr, N));
    }
}
