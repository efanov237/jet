import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;

public class GetNLargestTests {

    @Test
    public void getThirdLargest() {
        int[] arr = {42, 53, 17, 29, 64, 31};
        Assert.assertEquals(42, Main.getLargest(arr, 3));
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void getIndexOutOfBound () {
        Main.getLargest(new int[1], 10);
    }

}
